import iconBored from '../assets/icon_bored.png';
import iconEureka from '../assets/icon_eureka.png';
import iconExcited from '../assets/icon_excited.png';
import iconThoughtful from '../assets/icon_thoughtful.png';

export const SENTIMENTS = [
  {
    label:'Eureka!',
    icon: iconEureka,
    desc: 'You just had a eureka moment!',
    descLow: 'You just had a small eureka moment.',
    descHigh: 'You just had a great eureka moment!',
    color: 'red'
  },
  {
    label:'Excited',
    icon: iconExcited,
    desc: 'You are excited!',
    descLow: 'You are a little excited.',
    descHigh: 'You are very excited!',
    color: 'yellow'
  },
  {
    label:'Thoughtful',
    icon: iconThoughtful,
    desc: 'You are feeling thoughtful.',
    descLow: 'You are feeling a little thoughtful.',
    descHigh: 'You are feeling very thoughtful.',
    color: 'green',
  },
  {
    label:'Bored',
    icon: iconBored,
    desc: 'You are feeling bored.',
    descLow: 'You are feeling a little bored.',
    descHigh: 'You are feeling very bored.',
    color: 'blue',
  },
];
