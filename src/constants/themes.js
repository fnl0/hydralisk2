import { createMuiTheme } from '@material-ui/core/styles';

const commonTheme = {
  typography: {
    useNextVariants: true,
  },
};

export const darkTheme = createMuiTheme({
  ...commonTheme,

  palette: {
    type: 'dark',
  },
});

export const darkTabsTheme = createMuiTheme({
  ...commonTheme,

  palette: {
    type: 'dark',
    primary: {
      main: '#fff',
    },
  },
});

export const defaultTheme = createMuiTheme({
  ...commonTheme,

  palette: {
    primary: {
      light: '#b564be',
      main: '#83358d',
      dark: '#54005f',
      contrastText: '#fff',
    },

    secondary: {
      light: '#b6ffff',
      main: '#81d4fa',
      dark: '#4ba3c7',
      contrastText: '#000',
    },
  },
});
