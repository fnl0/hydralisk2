// For changing JSS insertion order
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from 'jss';
import { createGenerateClassName, jssPreset } from '@material-ui/core/styles';

import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import 'typeface-roboto';
import { defaultTheme } from './constants/themes';
import AppRouter from './routes/AppRouter';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { store } from './store/Store';

const generateClassName = createGenerateClassName();
const jss = create({
  ...jssPreset(),
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
  insertionPoint: document.getElementById('jss-insertion-point'),
});

momentDurationFormat(moment);

ReactDOM.render((
  <JssProvider jss={jss} generateClassName={generateClassName}>
    <Provider store={store}>
      <React.Fragment>
        <CssBaseline />
        <MuiThemeProvider theme={defaultTheme}>
          <AppRouter />
        </MuiThemeProvider>
      </React.Fragment>
    </Provider>
  </JssProvider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
