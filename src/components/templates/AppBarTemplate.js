import { withTheme } from '@material-ui/core/styles';
import React from 'react';
import styled from 'react-emotion';
import AppHeader from '../AppHeader';

const AppBarTemplateWrapper = styled('div')`
  background: ${({ theme: { palette } }) => `linear-gradient(90deg, ${palette.primary.dark}, ${palette.primary.main}`});
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

export default withTheme()(function AppBarTemplate ({ className, children, theme, headerProps = {} }) {
  return (
    <AppBarTemplateWrapper className={className} theme={theme}>
      <AppHeader {...headerProps} />
      {children}
    </AppBarTemplateWrapper>
  );
})
