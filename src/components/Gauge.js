import { animated, Spring } from 'react-spring'
import React from "react";

const TICK_ID = 'tick';

export default class Gauge extends React.Component {
  renderDial = (opts) => {
    const angleOffset = Math.min(
      Math.abs(opts.needleEndAngle - opts.needleStartAngle),
      360
    ) / 360;

    const radius = opts.radius - 22;
    const circumference = 2 * Math.PI * radius + 54;
    const minOffset = circumference * (1 - angleOffset) / angleOffset;
    const maxOffset = circumference;
    const offset = minOffset + (maxOffset - minOffset) * (1 - 1);

    return (
      <circle
        cx={opts.cX}
        cy={opts.cY}
        r={radius}
        fill='none'
        stroke={opts.tickColor}
        strokeWidth={opts.tickWidth}
        strokeDasharray={circumference}
        strokeDashoffset={offset}
        strokeLinecap={'butt'}
      />
    )
  };

  defineTick = (opts) => {
      let tX1 = opts.cX + opts.radius - (Math.max(opts.dialWidth, opts.progressWidth) / 2) - 15;
      let tX2 = tX1 - opts.tickLength - 4;

      return (<line
          id={TICK_ID}
          x1={tX1}
          y1={opts.cY}
          x2={tX2}
          y2={opts.cY}
          stroke={opts.tickColor}
          strokeWidth={opts.tickWidth}
      />);
  };

  renderTicks = (opts) => {
    let tickAngles = [];
    for (let i = 0; i <= opts.needleEndAngle - opts.needleStartAngle; i += opts.tickInterval) {
      tickAngles.push(i);
    }

    return (
      <g className='ticks'>
        {
          tickAngles.map((tickAngle, idx) => {
            return <use
              href={`#${TICK_ID}`}
              key={`tick-${idx}`}
              transform={`rotate(${tickAngle} ${opts.cX} ${opts.cY})`}
            />
          })
        }
      </g>
    )
  };

  renderProgress = (opts) => {
    const angleOffset = Math.min(
      Math.abs(opts.needleEndAngle - opts.needleStartAngle),
      360
    ) / 360;

    const minOffset = opts.circumference * (1 - angleOffset) / angleOffset;
    const maxOffset = opts.circumference;
    const offset = minOffset + (maxOffset - minOffset) * (1 - opts.currentValue);

    return (
      <Spring
        native
        to={{ offset, progressColor: opts.progressColor }}
      >
        {props =>
          <animated.circle
            cx={opts.cX}
            cy={opts.cY}
            r={opts.radius - 10}
            fill='none'
            stroke={props.progressColor}
            strokeWidth={opts.progressWidth}
            strokeDasharray={opts.circumference}
            strokeDashoffset={props.offset}
            strokeLinecap={opts.progressRoundedEdge ? 'round' : 'butt'}
          />
        }
      </Spring>
    );
  };

  renderNeedle = (opts) => {

      let
          x1 = opts.cX,
          y1 = opts.cY - (opts.needleWidth / 2),
          x2 = opts.cX,
          y2 = opts.cY + (opts.needleWidth / 2),
          x3 = opts.diameter,
          y3 = opts.cY,
          needleAngle = (360 * opts.currentValue) / 100;

      let needleElm = null;
      if (opts.needleSharp) {
          needleElm = (
              <polygon
                  points={`${x1},${y1} ${x2},${y2} ${x3},${y3}`}
                  fill={opts.needleColor}
              >

              </polygon>
          );
      } else {
          needleElm = (
              <line
                  x1={opts.cX}
                  y1={opts.cY}
                  x2={opts.diameter}
                  y2={opts.cY}
                  fill='none'
                  strokeWidth={opts.needleWidth}
                  stroke={opts.needleColor}
              />
          );
      }

      return (
          <g className='needle'>
              <g transform={`rotate(${needleAngle} ${opts.cX} ${opts.cY})`}>
                  {needleElm}
              </g>
              <circle
                  cx={opts.cX}
                  cy={opts.cY}
                  r={opts.needleBaseSize}
                  fill={opts.needleBaseColor}
              >
              </circle>
          </g>
      )

  };

  renderText = (opts) => {
    return (
      <text
        x={opts.cX}
        y={opts.cY + 55}
        fontFamily={opts.progressFont}
        fontSize={opts.progressFontSize}
        transform={`rotate(90 ${opts.cX} ${opts.cY})`}
        textAnchor="middle"
        fill={opts.progressColor}
      >
        {opts.currentValue}
      </text>
    )
  };

  renderFace(opts) {
    return (
      <circle
        cx={opts.cX}
        cy={opts.cY}
        r={opts.radius}
        fill='none'
      />
    )
  }

  render() {

    const { needleStartAngle, needleEndAngle } = this.props;

      let opts = Object.assign({}, this.props);

      let {
          size,
          dialWidth,
      } = opts;

      let cX = size / 2;
      let cY = size / 2;
      let radius = (size - (2 * dialWidth)) / 2;
      let diameter = 2 * radius;
      let circumference = 2 * Math.PI * radius;
      opts = Object.assign(opts, {
        cX,
        cY,
        radius,
        diameter,
        circumference,
        needleStartAngle,
        needleEndAngle,
      });
    // {this.renderNeedle(opts)}

      return (
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className={opts.className}
            viewBox={`0 0 ${size} ${size}`}
          >
            <defs>
              {this.defineTick(opts)}
            </defs>
            <g>
              {this.renderFace(opts)}
            </g>
            <g transform={`rotate(${needleStartAngle - 90} ${cX} ${cY})`}>
              {this.renderDial(opts)}
              {this.renderTicks(opts)}
              {this.renderProgress(opts)}
            </g>
          </svg>
      )
  }
}

Gauge.defaultProps = {
  size: 200,

  dialWidth: 10,
  dialColor: '#eee',

  tickLength: 0,
  tickWidth: 1,
  tickColor: '#cacaca',
  tickInterval: 10,

  currentValue: 0,
  progressWidth: 15,
  progressColor: '#3d3d3d',
  progressRoundedEdge: true,
  downProgressColor: 'red',
  progressFont: 'Serif',
  progressFontSize: '40',

  needle: true,
  needleBaseSize: 5,
  needleBaseColor: '#9d9d9d',
  needleWidth: 2,
  needleSharp: false,
  needleColor: '#8a8a8a',
  needleStartAngle: 0,
  needleEndAngle: 0,
};
