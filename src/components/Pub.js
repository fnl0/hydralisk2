import Card from '@material-ui/core/Card';
import LinearProgress from '@material-ui/core/LinearProgress';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import { css } from 'react-emotion';
import { EVENTS } from '../constants/events';
import { searchEvent } from '../utils/api';
import PubBody from './pub/PubBody';
import PubHeader from './pub/PubHeader';
import AppBarTemplate from './templates/AppBarTemplate';
import queryString from 'query-string';
import img1 from '../assets/img1.jpg';
import img2 from '../assets/img2.jpg';
import img3 from '../assets/img3.jpg';
import img4 from '../assets/img4.jpg';
import img5 from '../assets/img5.jpg';
import img6 from '../assets/img6.jpg';

const IMAGES = [img1, img2, img3, img4, img5, img6];

export default withWidth()(class Pub extends React.Component {
  state = {
    loaded: false,
    event: undefined,
  };

  componentDidMount() {
    const values = queryString.parse(this.props.location.search)
    const initialEventCode = values.eventCode;
    const eventCode = (initialEventCode || '').toUpperCase();
    const eventMatch = EVENTS.find(({ code }) => code.toUpperCase() === eventCode);

    if (eventMatch) {
      this.setState({ loaded: true, event: eventMatch });
      return;
    }

    searchEvent(eventCode)
      .then(({ data }) =>  {
        if (data && data.data && data.data[0]) {
          const randomIndex = Math.floor(Math.random() * Math.floor(6));
          const image = IMAGES[randomIndex];
          const event = {
            image,
            ...data.data[0],
          };
          this.setState({ event, loaded: true });
        }
      })
      .catch(console.error);
  }

  render() {
    const { width } = this.props;
    const { loaded, event }  = this.state;
    const isXsDown = isWidthDown('xs', width);

    return (
      <AppBarTemplate>
        {!loaded && <LinearProgress />}

        {loaded && (
          <div
            className={css`
              display: flex;
              flex-direction: column;
              padding: 0 ${isXsDown ? '10px' : '25px'};
            `}
          >
            <Card
              className={css`
                width: 100%;
                padding: ${isXsDown ? '10px 5px' : '20px 10px'};
                margin-top: 10px;
              `}
            >
              <PubHeader event={event} width={width} />
            </Card>

            <PubBody event={event} width={width} />
          </div>
        )}
      </AppBarTemplate>
    );
  }
})
