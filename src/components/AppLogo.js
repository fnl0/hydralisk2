import { withTheme } from '@material-ui/core/styles';
import React from 'react';
import styled, { css } from 'react-emotion';
import AppIcon from './icons/AppIcon';

const AppLogoWrapper = styled('div')`
  display: flex;
  align-items: center;
`;

export default withTheme()(function AppLogo ({ className, theme, withText }) {
  return (
    <AppLogoWrapper className={className}>
      <AppIcon />

      {withText && (
        <span
          className={css`
            margin-left: 0.5em;
            letter-spacing: 0.2em;
            font-size: 0.5em;
            font-family: ${theme.typography.fontFamily};
            font-weight: bold;
            text-transform: uppercase;
          `}
        >
          {process.env.REACT_APP_NAME}
        </span>
      )}
    </AppLogoWrapper>
  )
})
