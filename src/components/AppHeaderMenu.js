import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import MenuIcon from '@material-ui/icons/Menu';
import React from 'react';
import { darkTheme } from '../constants/themes';
import { css } from 'react-emotion';
import TextLink from './TextLink';

const MENU_ITEMS = [
  { label: 'Log in', link: '/sign-in' },
  { label: 'Register', link: '/register' },
  { label: 'Events', link: '/events' },
];

export default withWidth()(class AppHeaderMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render () {
    const { width } = this.props;
    const { anchorEl } = this.state;
    const { handleClick, handleClose } = this;

    return (
      <div
        className={css`
          display: flex;

          > * + * {
            margin-left: 15px;
          }
        `}
      >
        {isWidthUp('sm', width)
          ? (
            <MuiThemeProvider theme={darkTheme}>
              <React.Fragment>
                {MENU_ITEMS.map(({ label, link }) => (
                  <TextLink key={label} to={link}>
                    <Typography className={css`opacity: 0.6`}>{label}</Typography>
                  </TextLink>
                ))}
              </React.Fragment>
            </MuiThemeProvider>
          )
          : (
            <React.Fragment>
              <IconButton aria-label="Menu" color="inherit" onClick={handleClick}>
                <MenuIcon
                  className={css`
                    opacity: 0.6;
                  `}
                />
              </IconButton>

              <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
                PaperProps={{ style: { maxHeight: '90vh' } }}
              >
                {MENU_ITEMS.map(({ label, link }) => (
                  <MenuItem key={label}>
                    <TextLink to={link}>{label}</TextLink>
                  </MenuItem>
                ))}
              </Menu>
            </React.Fragment>
          )
        }
      </div>
    );
  }
})
