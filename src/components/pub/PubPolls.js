import Card from '@material-ui/core/Card';
import Fade from '@material-ui/core/Fade';
import RootRef from '@material-ui/core/RootRef';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import styled, { css, cx } from 'react-emotion';
import Measure from 'react-measure';
import { Spring } from 'react-spring'
import { defaultTheme } from '../../constants/themes';
import { SENTIMENTS } from '../../constants/sentiments';
import Gauge from '../Gauge';
import PubPollsResult from './PubPollsResult';

import ClickNHold from 'react-click-n-hold'; 
// eslint-disable-next-line
import cnhcss from '../../assets/cnh.css';


const PubQuestionsFooter = styled('div')`
  bottom: 0;
  display: flex;
  justify-content: space-around;
  left: 0;
  padding-bottom: 5px;
  position: fixed;
  right: 0;
`;

const SentimentButton = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
  user-select: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0); 
  -webkit-user-select: none;
  -webkit-touch-callout: none;
`;

const preventDefault = e => e.preventDefault();

const SHOW_METER_DELAY_SEC = 0.2;
const METER_CHARGE_DURATION_SEC = 1.8;
const HOLD_TO_MAX_DURATION_SEC = 0.5;
const SHOW_RESULT_DURATION_SEC = 1.5;
const LOW_SENTIMENT_THRESHOLD = 0.1;
const HIGH_SENTIMENT_THRESHOLD = 0.9;

export default class PubQuestions extends React.Component {
  state = {
    selectedSentiment: null,
    showMeter: false,
    showSentimentResult: null,
    startMs: null,
    sentimentValue: null,
    chartOuterDim: {},
  };

  showMeterTimeout = null;

  hideSentimentResultTimeout = null;

  start = (selectedSentiment) => (event) => {
    this.setState({
      selectedSentiment,
      startMs: new Date().valueOf(),
      showSentimentResult: null,
      sentimentValue: null,
    });

    this.clearTimeouts();
    this.showMeterTimeout = window.setTimeout(() => {
      this.setState({ showMeter: true });
    }, SHOW_METER_DELAY_SEC * 1000);
  };

  end = (sentiment) => (event, enough) => {
    this.clearTimeouts();
    
    const { selectedSentiment, startMs } = this.state;

    const sentimentValue =
      Math.max(
        Math.min(
          (new Date().valueOf() - startMs) / 1000 /
          (METER_CHARGE_DURATION_SEC + SHOW_METER_DELAY_SEC + HOLD_TO_MAX_DURATION_SEC),
          1,
        ),
        0,
      );

    this.setState({
      showMeter: false,
      startMs: null,
      selectedSentiment: null,
      showSentimentResult: selectedSentiment,
      sentimentValue,
    });

    this.hideSentimentResultTimeout = window.setTimeout(() => {
      this.setState({ showSentimentResult: null, sentimentValue: null });
    }, SHOW_RESULT_DURATION_SEC * 1000);
  };

  onClickNHold = (sentiment) => (event, enough) => {
  };

  clearTimeouts() {
    if (this.showMeterTimeout) {
      window.clearTimeout(this.showMeterTimeout);
      this.showMeterTimeout = null;
    }

    if (this.hideSentimentResultTimeout) {
      window.clearTimeout(this.hideSentimentResultTimeout);
      this.hideSentimentResultTimeout = null;
    }
  }

  componentWillUnmount() {
    this.clearTimeouts();
  }
  
  render() {
    const { className, width } = this.props;
    const {
      selectedSentiment,
      showMeter,
      showSentimentResult,
      sentimentValue,
      chartOuterDim,
    } = this.state;
    const isXsDown = isWidthDown('xs', width);

    return (
      <MuiThemeProvider theme={defaultTheme}>
        <Measure
          bounds
          onResize={contentRect => {
            this.setState({ chartOuterDim: contentRect.bounds })
          }}
        >
          {({ measureRef }) => (
            <RootRef rootRef={measureRef}>
              <Card
                className={cx(
                  css`
                    padding: ${isXsDown ? '10px 5px' : '20px 10px'};
                    min-height: 500px;
                    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
                  `,
                  className
                )}
              >
                <PubPollsResult
                  initialDim={chartOuterDim}
                  pause={showMeter}
                  showSentimentResult={showSentimentResult}
                  sentimentValue={sentimentValue}
                />
              </Card>
            </RootRef>
          )}
        </Measure>

        {(showMeter || showSentimentResult) && (
          <Fade in>
            <div
              className={css`
                background: linear-gradient(
                  0deg,
                  rgba(0, 0, 0, 0),
                  rgba(0, 0, 0, 0.5) 80px,
                  rgba(0, 0, 0, 0.5)
                );
                position: fixed;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
              `}
            />
          </Fade>
        )}

        {showMeter && (
          <Spring
            from={{ value: 0, progressColor: '#F55' }}
            to={{ value: 1, progressColor: '#5F5' }}
            config={{ duration: METER_CHARGE_DURATION_SEC * 1000 }}
          >
            {props =>
              <Gauge
                className={css`
                  position: fixed;
                  top: 50%;
                  left: 50%;
                  transform: translate(-50%, -50%);
                  width: calc(100vw - 10px);
                  max-height: calc(100vh - 80px * 2);
                  max-width: 500px;
                `}
                needleStartAngle={-135}
                needleEndAngle={135}
                tickInterval={27}
                progressColor={props.progressColor}
                currentValue={props.value}
              />
            }
          </Spring>
        )}

        {!!showSentimentResult && (
          <Fade in>
            <div
              className={css`
                position: fixed;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                width: calc(100vw - 10px);
                max-height: calc(100vh - 80px * 2);
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
              `}
            >
              <img
                className={css`width: 150px;`}
                src={showSentimentResult.icon}
                alt={`${showSentimentResult.label} icon`}
              />

              <Typography
                variant="h5"
                className={css`color: #fff; margin-top: 5px;`}
                align="center"
              >
                {(sentimentValue >= HIGH_SENTIMENT_THRESHOLD && showSentimentResult.descHigh) ||
                    (sentimentValue <= LOW_SENTIMENT_THRESHOLD && showSentimentResult.descLow) ||
                    showSentimentResult.desc
                }
              </Typography>
            </div>
          </Fade>
        )}

        <PubQuestionsFooter screenWidth={width}>
          {SENTIMENTS.map(sentiment => (
            <SentimentButton
              key={sentiment.label}
            >
              <ClickNHold 
                time={METER_CHARGE_DURATION_SEC}
                onStart={this.start(sentiment)}
                onClickNHold={this.onClickNHold(sentiment)}
                onEnd={this.end(sentiment)}
              >
                <img
                  src={sentiment.icon}
                  alt={`${sentiment.label} icon`}
                  className={css`
                    display: block;
                    width: 50px;
                    user-select: none;
                    -webkit-user-select:none;
                    -webkit-touch-callout:none;
                  `}
                  onContextMenu={preventDefault}
                  onDragStart={preventDefault}
                />
              </ClickNHold>

              <Typography
                variant="caption"
                className={css`
                  color: ${selectedSentiment === sentiment ? '': '#FFF' };
                  text-shadow: #000 1px 1px 3px;
                  user-select: none;
                  -webkit-user-select: none;
                  -webkit-touch-callout: none;
                `}
                color={selectedSentiment === sentiment ? 'secondary' : undefined}
                onContextMenu={preventDefault}
              >
                {sentiment.label}
              </Typography>
            </SentimentButton>
          ))}
        </PubQuestionsFooter>
      </MuiThemeProvider>
    );
  }
  }
