import { List } from 'immutable';
import moment from 'moment';
import React from 'react';
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from 'recharts';
import { SENTIMENTS } from '../../constants/sentiments';
import { formatDuration } from '../../utils/dates';

const TIME_NAME = 'Time';
const EMOTION_NAME = 'Emotion';

const randomSentiment = () =>
  SENTIMENTS[Math.round(Math.random() * (SENTIMENTS.length - 1))];

const now = 10000;

const timeFormatter = (i) => formatDuration(moment.duration(i));

const emotionFormatter = i => {
  const emotionValue = i * 100;

  return `${emotionValue % 1 ? emotionValue.toFixed(2) : emotionValue}%`;
};

const tooltipFormatter = (value, name, props) => {
  switch (name) {
    case TIME_NAME:
      return timeFormatter(value);

    case EMOTION_NAME:
      return emotionFormatter(value);

    default:
      return value;
  }
}

export default class PubPollsResult extends React.Component {
  state = {
    data: List([
      {x: now - 1000, y: 0.1, z: randomSentiment().label},
    ]),
  };

  dataInterval = null;

  componentDidMount() {
    this.dataInterval = setInterval(() => {
      if (this.props.pause) {
        return;
      }

      if (Math.random() > 0.5) {
        return;
      }

      const newTime = this.state.data.last().x + Math.random() * 15000;

      this.setState({
        data: this.state.data.push({
          x: newTime,
          y: Math.random(),
          z: Math.random() >=  0.2
            ? this.state.data.last().z
            : randomSentiment().label,
        })
      });
    }, 10000);
  }

  componentDidUpdate(prevProps) {
    // Hack to add new Sentiment
    if (!prevProps.sentimentValue && this.props.sentimentValue > 0 && this.props.showSentimentResult) {
      this.setState({
        data: this.state.data.push({
          x: this.state.data.last().x + 1000,
          y: this.props.sentimentValue,
          z: this.props.showSentimentResult.label,
        }),
      });
    }
  }

  componentWillUnmount() {
    if (this.dataInterval) {
      clearInterval(this.dataInterval);
    }
  }

  render () {
    const { initialDim } = this.props;
    const { data } = this.state;
    const dataArray = data.toArray();

    return (
      <ScatterChart
        width={initialDim.width - 10}
        height={450}
        margin={{top: 20, right: 20, bottom: 20, left: 20}}
      >
        <XAxis
          type="number"
          dataKey="x"
          name='Time'
          scale="time"
          tickFormatter={timeFormatter}
          domain={['dataMin', 'dataMax']} 
        />
        <YAxis
          type="number"
          dataKey={'y'}
          name='Emotion'
          tickFormatter={emotionFormatter}
        />
        <CartesianGrid />
        <Legend />
        <Tooltip
          cursor={{strokeDasharray: '3 3'}}
          formatter={tooltipFormatter}
        />

        {SENTIMENTS.map(({ label, color }) => (
          <Scatter
            key={label}
            name={label}
            data={dataArray.filter(({ z }) => z === label)}
            fill={color}
          />
        ))}
      </ScatterChart>
    );
  }
}
