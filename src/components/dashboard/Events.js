import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';
import Admin from './Admin'
import {withStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography/Typography";
import Button from "@material-ui/core/Button/Button";
import axios from "axios";
import EventTable from "./EventTable";

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  createButton: {
    marginBottom: theme.spacing.unit * 3,
  },
});

class Events extends React.Component {

  state = {
    events: []
  };

  componentDidMount() {
    axios.get(`${process.env.REACT_APP_BACKEND}/events?$sort[createdAt]=-1`)
      .then(response => {
        this.setState({events: response.data.data})
      })
      .catch(error => {
        console.log(error);
      })
  }

  render() {
    const { classes } = this.props;
    const { events } = this.state;
    return (
      <Admin>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Typography variant="h4" gutterBottom component="h2">
            Events
          </Typography>
          <Button color="primary" className={classes.createButton} variant="contained" component={Link} to="/event">Create Event</Button>
          <EventTable events={events}/>
        </main>
      </Admin>
    );
  }
}

Events.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Events);
