import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Admin from './Admin'
import Paper from "@material-ui/core/Paper/Paper";
import SimpleLineChart from "./SimpleLineChart";

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  textField: {
    width: 300,
  },
});

class View extends React.Component {
  render() {
    const {classes} = this.props;
    return (
      <Admin>
        <main className={classes.content}>
          <div className={classes.appBarSpacer}/>
          <Typography variant="h4" gutterBottom component="h2">
            Sentiments Graphs
          </Typography>
          <Paper className={classes.paper}>
            <SimpleLineChart/>
          </Paper>
        </main>
      </Admin>
    );
  }
}

View.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(View);
