import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Admin from "./Admin";
import axios from "axios";
import UserTable from "./UserTable";
import Typography from "@material-ui/core/Typography/Typography";

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  }
});

const getUsers = () => {
  return axios.get('/api/users?$limit=500&$sort[name]=1');
};

const getProfiles = () => {
  return axios.get('/api/profiles?$limit=10000');
};

class Users extends React.Component {

  state = {
    users: []
  };

  componentDidMount() {
    axios.all([getUsers(), getProfiles()])
      .then(axios.spread((users, profiles) => {
        this.setState(
          {
            users: users.data.data,
            profiles: profiles.data.data,
          })
      }));
  }

  render() {
    const {classes} = this.props;
    const {users, profiles} = this.state;
    return (
      <Admin>
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Typography variant="h4" gutterBottom component="h2">
            Users
          </Typography>
          <UserTable users={users} profiles={profiles}/>
        </main>
      </Admin>
    )
  }
}

Users.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Users);
