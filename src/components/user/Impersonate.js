import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import deepOrange from '@material-ui/core/colors/deepOrange';
import deepPurple from '@material-ui/core/colors/deepPurple';
import blue from '@material-ui/core/colors/blue';
import amber from '@material-ui/core/colors/amber';
import classNames from 'classnames';
import client from "../../feathers";
import {withRouter} from "react-router-dom";

const styles = {
  avatar: {
    margin: 10,
    width: 75,
    height: 75,
  },
  orangeAvatar: {
    color: '#fff',
    backgroundColor: deepOrange[500],
  },
  purpleAvatar: {
    color: '#fff',
    backgroundColor: deepPurple[500],
  },
  blueAvatar: {
    color: '#fff',
    backgroundColor: blue[500],
  },
  amberAvatar: {
    color: '#fff',
    backgroundColor: amber[500],
  },
  row: {
    display: 'flex',
    justifyContent: 'center',
  },
};

const impersonator = {
  paul: {
    email: 'paul@fnl0.com', password: 'secret', strategy: 'local'
  },
  admin: {
    email: 'admin@fnl0.com', password: 'admin', strategy: 'local'
  },
  klee: {
    email: 'klee@fnl0.com', password: 'secret', strategy: 'local'
  },
  max: {
    email: 'max@fnl0.com', password: 'secret', strategy: 'local'
  },
};

class Impersonate extends React.Component {

  login = (user) => {
    client.logout();
    const payload = impersonator[user];
    client.authenticate(payload)
      .then(response => {
        return client.passport.verifyJWT(response.accessToken);
      })
      .then(payload => {
        return client.service('api/users').get(payload.userId);
      })
      .then(user => {
        client.set('user', user);
      })
      .then(() => {
        this.props.history.push('/events');
      })
      .catch(error => {
        this.setState({ error })
      });
  };

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.row}>
        <Avatar onClick={() => this.login('admin')}
                className={classNames(classes.avatar, classes.amberAvatar)}>Admin</Avatar>
        <Avatar onClick={() => this.login('klee')} className={classNames(classes.avatar, classes.orangeAvatar)}>Klee</Avatar>
        <Avatar onClick={() => this.login('paul')} className={classNames(classes.avatar, classes.purpleAvatar)}>Paul</Avatar>
        <Avatar onClick={() => this.login('max')} className={classNames(classes.avatar, classes.blueAvatar)}>Max</Avatar>
      </div>
    );
  }
}

Impersonate.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Impersonate));
