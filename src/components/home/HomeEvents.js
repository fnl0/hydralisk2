import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Card from '@material-ui/core/Card';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { formatEventDate } from '../../utils/dates';
import React from 'react';
import styled, { css } from 'react-emotion';
import { withRouter } from 'react-router-dom';
import { EVENTS } from '../../constants/events';
import { darkTheme } from '../../constants/themes';
import EventThumbnail from '../EventThumbnail';

const HomeEventsWrapper = styled('div')`
  align-items: center;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;


export default withRouter(class HomeEvents extends React.Component {
  onClickItem = (code) => () => {
    this.props.history.push(`/client?eventCode=${code}`);
  };

  render() {
    const { className } = this.props;
    const { onClickItem } = this;

    return (
        <HomeEventsWrapper className={className}>
          <div>
            <MuiThemeProvider theme={darkTheme}>
              <div className={css`text-transform: uppercase;`}>
                <Typography variant="h6">Today</Typography>
              </div>
            </MuiThemeProvider>

            <Card className={css``}>
              <List>
                {EVENTS.map(({ code, title, speaker, start, end, image }, i) => (
                  <ListItem
                    key={i}
                    className={css`
                      align-items: flex-start;
                      cursor: pointer;
                      display: flex;
                    `}
                    divider={i < EVENTS.length - 1}
                    onClick={onClickItem(code)}
                  >
                    <EventThumbnail imageUrl={image} imageSize="80px" />

                    <div
                      className={css`
                        margin-left: 10px;
                        flex: 1;
                      `}
                    >
                      <ListItemText primary={title} secondary={`By ${speaker}`}/>
                      <div
                        className={css`
                          display: flex;
                          justify-content: space-between;
                          flex-wrap: wrap;
                        `}
                      >
                        <Typography className={css`margin-right: 10px;`}>Start: {formatEventDate(start)}</Typography>
                        <Typography>End: {formatEventDate(end)}</Typography>
                      </div>
                    </div>
                  </ListItem>
                ))}
              </List>
            </Card>
          </div>
        </HomeEventsWrapper>
    );
  }
})
