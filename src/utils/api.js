import axios from 'axios';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import { defaultMemoize } from 'reselect';
import io from 'socket.io-client';

const BASE_URL = process.env.REACT_APP_BACKEND;

export const searchEvent = code => axios.get(`${BASE_URL}/events?code=${code}`);

export const addQuestion = (content, { eventCode }) =>
  axios.post(`${BASE_URL}/questions`, { content, upvotes: 0, upvotesByUserId: {}, eventCode });

export const upvoteQuestion = question => {
  axios.put(`${BASE_URL}/questions/${question._id}`, { ...question, upvotes: question.upvotes + 1 });
};

const configureSocket = defaultMemoize(() => {
  const socket = io('/');
  const app = feathers();
  app.configure(socketio(socket));

  return { app, socket };
});

export const subscribeService = (serviceName, onChange) => {
  if (process.env.NODE_ENV === 'test') {
    return;
  }

  const { app } = configureSocket();
  const service = app.service(`${BASE_URL}/${serviceName}`);

  let items = { byId: {} };

  const handleChange = () => {
    items = {...items};
    onChange(items);
  };

  service.find().then(({ data }) => {
    data.forEach(item => {
      items.byId[item._id] = item;
    });
    handleChange();
  }).catch(console.error);

  const onCreateOrUpdate = item => {
    items.byId[item._id] = item;
    handleChange();
  };

  service
    .on('created', onCreateOrUpdate)
    .on('updated', onCreateOrUpdate)
    .on('patched', onCreateOrUpdate);

  return {
    unsubscribe: () => {
      service.removeListener('created', onCreateOrUpdate);
      service.removeListener('updated', onCreateOrUpdate);
    },
  };
};
